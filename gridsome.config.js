// This is where project configuration and plugin options are located. 
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: 'u.baldir.fr',
  siteDescription: 'Baldir.fr url shortener',

  plugins: [
    {
      // Create posts from markdown files
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'Url',
        path: 'content/links/*.yml',
        route: '/:tinyUrl'
      }
    }

  ]
}
