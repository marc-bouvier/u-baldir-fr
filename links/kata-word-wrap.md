---
redirect_to: https://github.com/emilybache/WordWrap-TestDesign-Kata
title: "Word Wrap Test Design Kata " 
description: "This repo contains several implementations of the [Word Wrap Kata](https://sammancoaching.org/kata_descriptions/word_wrap.html). There is one test to start you off, that tests all the implementations. Write further tests so you cover all the interesting functionality and edge cases. You should be able to write tests in such a way that the same tests will exercise all the various implementations."
---
