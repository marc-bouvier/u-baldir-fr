---
redirect_to: https://github.com/swcraftstras/swcraftstras.github.io/discussions/6
title: "Software Crafters Strasbourg - 👍 Upvotez les thématiques qui vous intéressent" 
description: "Nous sommes toujours à la recherche de nouveaux sujets et de personnes ayant des présentations à partager pour la communauté. Aidez nous en nous indiquant quelles thématiques vous intéressent sur chacune des discussion de ce topic."
---
