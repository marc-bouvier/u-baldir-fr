---
redirect_to: https://gitlab.svc.agpm.fr/ged-workflow/exemples/vuejs-3/-/tree/main/WordWrap-TestDesign-Kata
title: "Message Renderer Kata" 
description: "There are several test cases for the MessageRenderer class. Review them and work out which test design principles they do or don't follow."
---


