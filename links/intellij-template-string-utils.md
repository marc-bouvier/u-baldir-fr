---
redirect_to: https://github.com/JetBrains/intellij-community/blob/master/platform/util/src/com/intellij/openapi/util/text/StringUtil.java
title: "IntelliJ StringUtil" 
description: "Used for custom templates definitions like
<ul>
<li>getters  / setters
</li>
<li>toString
</li>
<li>builder
</li>
</ul>"
---
