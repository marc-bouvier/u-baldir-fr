---
redirect_to: "https://web.archive.org/web/20101224044646/http://sencore.com/monitoring.html"
title: "2011 Internet archive of Sencore.com"
description: "Internet archive of Sencore.com when I used to work on it. This was part of my internship and contract with <a href='https://hindsiteinc.com/'>Hindsite Interactive Inc</a> leaded by <a href='https://www.linkedin.com/in/paymantaei/'>Payman Taei</a>."
---
