---
redirect_to: "https://github.com/marc-bouvier/awesome-resources/"
title: "Awesome resources"
description: "List of awesome resources (mostly technical)"
---