---
redirect_to: "https://baldir-fr.github.io/bbl-docker-pour-le-developpeur"
title: "BBL — Docker sur le poste du développeur (slides)"
description: "Vous avez entendu parler de Docker, vous avez été formé dessus mais vous ne savez pas à quoi ça pourrait servir dans votre quotidien de développeur? Dans cette présentation vous verrez - Des cas d’utilisation concrets (ex. test containers, frontend + keycloak + spring boot, test de jobs CI, ...) - Des exemples / démos de certains de ces usages La présentation sera suivie d’une session de questions-réponses."
---
