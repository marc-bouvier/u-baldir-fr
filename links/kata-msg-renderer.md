---
redirect_to: https://github.com/emilybache/MessageRenderer-Test-Design-Kata
title: "Message Renderer Kata" 
description: "There are several test cases for the MessageRenderer class. Review them and work out which test design principles they do or don't follow."
---
