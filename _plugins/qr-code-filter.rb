require "rqrcode"

module Jekyll
  module QrCodeFilter
    def qr_code(url)
      url_svg_qrcode(url)
    end

    private def url_svg_qrcode(url)
      RQRCode::QRCode
        .new(url)
        .as_svg(
          fill: "ffffff",
          color: "000",
          shape_rendering: "crispEdges",
          module_size: 11,
          standalone: true,
          use_path: true)
    end
  end
end

Liquid::Template.register_filter(Jekyll::QrCodeFilter)