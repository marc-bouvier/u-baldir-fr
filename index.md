---
layout: default
title: "Baldir URL Shortener"
---

{% assign redirects = site.pages | where_exp: "item", "item.redirect_to != nil" %}
{% for page in redirects %}
## [{{ page.url }}]({{ page.url | relative_url }})

> {{ page.title | escape }}
> 
> {{ page.description }}

{::nomarkdown}
{{ page.url | relative_url | qr_code }}
{:/}

`{{ page.redirect_to }}`

***

{% endfor %}