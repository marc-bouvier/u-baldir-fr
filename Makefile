all: clean build

# Depends on jekyll pre-requisites to be installed
#   -> https://jekyllrb.com/docs/installation/#guides

changes=git diff-index --quiet HEAD
changes_detected=0

# U_BALDIR_FR_ARTIFACT_URL environment var must be defined in order to run the "deploy" target
TMP_U_BALDIR_FR_DIR=/tmp/u-baldir.fr
U_BALDIR_FR_DEPLOY_DIR=_site/

fetch:
	git fetch

detect_changes: fetch
	$(shell ${changes})
ifneq (0,$?)
	@echo changes detected
else
	@echo no changes detected
endif

checkout: detect_changes
	$(shell ${changes})
ifneq (0,$?)
	git reset --hard origin/master
endif

install:
	bundle

build: install
	bundle exec jekyll build

clean:
	bundle exec jekyll clean

deploy:
	rm -rf ${TMP_U_BALDIR_FR_DIR}
	mkdir -p ${TMP_U_BALDIR_FR_DIR}
	curl --location --output ${TMP_U_BALDIR_FR_DIR}/artifacts.zip "${U_BALDIR_FR_ARTIFACT_URL}"
	unzip	-o ${TMP_U_BALDIR_FR_DIR}/artifacts.zip -d ${TMP_U_BALDIR_FR_DIR}/unzipped
	rm -rf ${U_BALDIR_FR_DEPLOY_DIR}
	mkdir -p ${U_BALDIR_FR_DEPLOY_DIR}
	cp -Rf ${TMP_U_BALDIR_FR_DIR}/unzipped/_site/* ${U_BALDIR_FR_DEPLOY_DIR}
	rm -rf ${TMP_U_BALDIR_FR_DIR}

serve:
	bundle exec jekyll serve
