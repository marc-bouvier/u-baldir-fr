changes=git diff-index --quiet HEAD
changes_detected=0

fetch:
	git fetch

detect_changes: fetch
	$(shell ${changes})
ifneq (0,$?)
	@echo changes detected
else
	@echo no changes detected
endif

checkout: detect_changes
	$(shell ${changes})
ifneq (0,$?)
	git reset --hard origin/master
endif

install:
	npm install

build: checkout install
	npm run build

all: build

clean:
	rm -rf node_modules
