# Url Shortener based on Jekyll

## Create a new short URL

Add a new markdown file in `links/`

Fill the frontmatter of the file
- `redirect_to`: the long URL to be shortened
- `title`: short description
- `description`: (optional) long description

```md
---
redirect_to: "https://github.com/marc-bouvier-katas/Kotlin_EduTools_Advent_of_Code_2021"
title: "Advent of code 2021 (unofficial) in Kotlin for Educational Plugin on Jetbrains IntelliJ IDE."
description: "Advent of code 2021 (unofficial) in Kotlin for Educational Plugin on Jetbrains IntelliJ IDE."
---
```

The slug of the short URL will match the file name.

Ex. `at-2022-ld.md` -> `your_url/at-2022-ld/`

## Run locally with Jekyll

Install expected ruby version (see [`Gemfile`](Gemfile)). You can do it with [rvm](https://rvm.io/).

```shell
# runs on http://localhost:4000
bundle exec jekyll serve
# remove _site folder and jekyll cache
bundle exec jekyll clean
# build site
bundle exec jekyll build
```

## Run locally with Makefile

```shell
# clean and build site
make
# runs on http://localhost:4000
make serve
# remove _site folder and jekyll cache
make clean
# build site
make build
```

## Run locally with Docker Compose

```shell
# runs on http://localhost:4000
docker compose up -d
# stop the server
docker compose down
```

You can also override command to run them specifically.

```shell
# runs on http://localhost:4000
docker compose run url-shortener bundle exec jekyll serve
# remove _site folder and jekyll cache
docker compose run url-shortener bundle exec jekyll clean
# build site
docker compose run url-shortener bundle exec jekyll build

It is also possible to use make commands

# clean and build site
docker compose run url-shortener make
# runs on http://localhost:4000
docker compose run url-shortener make serve
# remove _site folder and jekyll cache
docker compose run url-shortener make clean
# build site
docker compose run url-shortener make build
```
